#!/usr/bin/env python3
# -*- encode: utf-8 -*-

from pathlib import Path
from astropy.time import Time, TimeDelta
from astropy.units import Unit
from tfcat import Feature, Polygon, CRS, FeatureCollection, dump
import numpy
from astroquery.vizier import Vizier

VIZIER_CATALOG_ID = "J/ApJ/710/L58"
OBS_FACILITY_STRING = "Learmonth Solar Radio Observatory"
SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0.json"


def load_viz_data(viz_id=VIZIER_CATALOG_ID):
    viz_data = Vizier(catalog=viz_id, columns=['**']).query_constraints()[0]
    return viz_data


def resolve_observatory(obsname=OBS_FACILITY_STRING):
    import requests
    service_url = "https://voparis-elasticsearch.obspm.fr/obsfacility/resolve?q="
    result = requests.get(f"{service_url}{obsname}").json()

    facility_name = result['results'][0]['label']
    return facility_name


def event_converter(data):
    events = set([seq for seq in data['Seq'] if seq])
    for seq in events:
        seq_data = data[data['Seq'] == seq]
        for item in seq_data:
            # print(seq_data.colnames)
            tmin = Time(f"{item['Obs']}T{item['Stime']}Z")
            if item['Etime'].startswith('24'):
                tmax = Time(f"{item['Obs']}T00{item['Etime'][2:]}Z") + TimeDelta(1 * Unit("day"))
            else:
                tmax = Time(f"{item['Obs']}T{item['Etime']}Z")
            fmin = item['Sfreq'] * Unit("MHz")
            fmax = item['Efreq'] * Unit("MHz")

            contour = Polygon.from_bbox((tmin.unix, tmax.unix), (fmin.value, fmax.value))
            seq_id = f"{item['Seq']:02d}{item['m_Seq']}"

            yield Feature(
                id=seq_id,
                geometry=contour,
                properties={
                    "intensity": item['Int'],
                    "nb_true_positive": item['True'] if isinstance(item['True'], numpy.uint8) else None,
                    "nb_false_alarm": item['False'] if isinstance(item['False'], numpy.uint8) else None,
                }
            )


def converter(viz_id=VIZIER_CATALOG_ID):
    """
    Converts the input data into a TFCat Feature Collection

    :param viz_id: Vizier catalogue ID
    :return: tuple with a dictionary with FeatureCollection values, a year list and a file list
    """

    print(f"Processing catalogue: {VIZIER_CATALOG_ID}")
    # define the global properties
    properties = {
        "instrument_host_name": resolve_observatory(),
        "instrument_name": "Solar Radio Spectrograph",
        "target_name": "Sun",
        "target_class": "star",
        "target_region": "heliosphere",
        "feature_name": "radio emissions",
        "title": "Coronal type II radio bursts in 2002",
        "authors": [{
            "FamilyName": "Lobzin",
            "GivenName": "V.V.",
            "ORCID": "https://orcid.org/0000-0001-5655-9928"
        }, {
            "FamilyName": "Cairns",
            "GivenName": "I.H.",
            "ORCID": "https://orcid.org/0000-0001-6978-9765"
        }, {
            "FamilyName": "Robinson",
            "GivenName": "P.A.",
        }, {
            "FamilyName": "Steward",
            "GivenName": "G."
        }, {
            "FamilyName": "Patterson",
            "GivenName": "G."
        }],
        "bib_reference": "https://doi.org/10.1088/2041-8205/710/1/L58",
        "data_source_reference": "https://doi.org/10.26093/cds/vizier.17109058",
        "publisher": "Centre de Donnees astronomique de Strasbourg (CDS)",
    }

    # define fields
    fields = {
        'intensity': {
            'info': 'Intensity',
            'datatype': 'int',
            'ucd': 'meta.code'
        },
        'nb_true_positive': {
            'info': 'Number of true positive in daily spectra',
            'datatype': 'int',
            'ucd': 'meta.code'
        },
        'nb_false_alarm': {
            'info': 'Number of false alarm in daily spectra',
            'datatype': 'int',
            'ucd': 'meta.code'
        }
    }

    # define Coordinate Reference System
    crs = CRS.configure(
        time_coords_id="unix",
        spectral_coords_id="MHz",
        ref_position_id=resolve_observatory()
    )

    viz_data = load_viz_data(viz_id)

    collection = FeatureCollection(
        features=list(event_converter(viz_data)),
        properties=properties,
        fields=fields,
        crs=crs
    )

    tfcat_file = Path(__file__).parent / "build" / "vizier_tfcat_lobzin_2012.json"
    return collection, tfcat_file


def writer(viz_id):
    """writes out a TFCat JSON file

    :param viz_id: Vizier Catalogue ID
    """

    collection, tfcat_file = converter(viz_id)

    print(f"Writing file: {tfcat_file.name}")
    tfcat_file.parent.mkdir(parents=True, exist_ok=True)
    with open(tfcat_file, 'w') as f:
        dump(collection, f)
    print(f"Validating file: {tfcat_file.name}")
    validate(tfcat_file)


def validate(file_name):
    from tfcat.validate import validate_file
    validate_file(file_name, schema_uri="https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0.json")


if __name__ == "__main__":
    writer(VIZIER_CATALOG_ID)
