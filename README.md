# Lobzin et al 2012 - Vizier catalogue

This script has been used to produce the TFCat version of Lobzin et al. (2012) Vizier
catalogue (J/ApJ/710/L58).

## Run the example

```bash
python prepare_tfcat.py
```

## References

- Lobzin, Vasili V.; Cairns, Iver H.;  Robinson, Peter A.;  Steward, Graham;  Patterson, Garth. 
  (**2010**) *Automatic Recognition of Coronal Type II Radio Bursts: The Automated Radio Burst 
  Identification System Method and First Observations*. Astrophys. J. Lett., 710(1), L58-L62.
  [doi:10.1088/2041-8205/710/1/L58](https://doi.org/10.1088/2041-8205/710/1/L58), 
  [bibcode:2010ApJ...710L..58L](https://ui.adsabs.harvard.edu/abs/2010ApJ...710L..58L).
- Lobzin V.V., Cairns I.H., Robinson P.A., Steward G., Patterson G. (**2012**) *Coronal type II 
  radio bursts in 2002: J/ApJ/710/L58*. Vizier (CDS). 
  [doi:10.26093/cds/vizier.17109058](https://doi.org/10.26093/cds/vizier.17109058),

